

//
//  RootViewController.swift
//  Coffee Ratio
//
//  Created by rlogical-mac-01 on 27/06/19.
//  Copyright © 2019 John Peden. All rights reserved.
//

import UIKit

class RootViewController: UIViewController {

    @IBOutlet weak var btnGrams: UIButton!
    @IBOutlet weak var btnTBSP: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        btnGrams.layer.borderWidth = CGFloat(1.0)
        btnGrams.layer.borderColor = UIColor.white.cgColor
        btnGrams.layer.cornerRadius = 4.0
        btnGrams.layer.masksToBounds = true
        
        btnTBSP.layer.borderWidth = CGFloat(1.0)
        btnTBSP.layer.borderColor = UIColor.white.cgColor
        btnTBSP.layer.cornerRadius = 4.0
        btnTBSP.layer.masksToBounds = true


        // Do any additional setup after loading the view.
    }
    
    @IBAction func grams(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ViewController") as! ViewController
        vc.selectedValue = 0
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func tbsp(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ViewController") as! ViewController
        vc.selectedValue = 0
        self.navigationController?.pushViewController(vc, animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
