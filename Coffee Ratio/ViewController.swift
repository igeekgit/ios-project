
import UIKit

class ViewController: UIViewController {
    
    @IBOutlet var coffeeTF: UITextField!
    @IBOutlet var coffeeRatioTF: UITextField!
    @IBOutlet var waterRatioTF: UITextField!
    @IBOutlet var waterGramsLabel: UILabel!
    @IBOutlet var displayTimeLabel: UILabel!
    
    var timer = JMPTimer()
    var waterValues: [String]?
    var coffeeValues: [String]?
    var selectedValue = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        waterValues = ["18.5", "0.375"]
        coffeeValues = ["1", "1"]
        
        
        timer = JMPTimer(withLabel: displayTimeLabel)
        
        
        let myColor : UIColor = UIColor( red: 255, green: 255, blue:255, alpha: 1.0 )
        coffeeTF.layer.borderColor = myColor.cgColor
        coffeeTF.layer.borderWidth = 1
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(ViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
        
        
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    @objc func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    
    func calculateWaterInGrams(coffee: Float, water: Float) -> Float {
        return coffee * water
    }
    
    @IBAction func editingChanged(_ sender: UITextField) {
        if ((coffeeTF.text != nil) && (coffeeRatioTF.text != nil) && (waterRatioTF.text != nil)) {
            //let waterGrams = ((waterRatioTF.text! as NSString).floatValue) * ((coffeeTF.text! as NSString).floatValue)
            
            let coffeeLabelAsFloat = (coffeeTF.text! as NSString).floatValue
            let waterValueAsFloat = (waterRatioTF.text! as NSString).floatValue
            let waterGrams = calculateWaterInGrams(coffee: coffeeLabelAsFloat, water: waterValueAsFloat)
            
            waterGramsLabel.text = NSString(format: "%.2f", waterGrams) as String
            
            if waterGrams == 0.0 {
                waterGramsLabel.text = ""
            }
        }
    }
    
    @IBAction func start(_ sender: AnyObject) {
        timer.start()
    }
    
    @IBAction func stop(_ sender: AnyObject) {
        timer.stop()
    }
    
    @IBAction func clear() {
        timer.clear()
    }
    
    
    @IBAction func segmentedControlChanged(_ sender: UISegmentedControl) {
        waterRatioTF.text = waterValues?[sender.selectedSegmentIndex]
    }
    
    
}

