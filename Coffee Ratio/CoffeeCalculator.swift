
import Foundation

class CoffeeCalculator {
  /* Variables */
  var gramsOfWater: Double
  init() {
    gramsOfWater = 18.5
  }
  
  init(gramsOfWater: Double) {
    self.gramsOfWater = gramsOfWater
  }
  
  func calculateGramsOfWaterTimes(gramsOfCoffee: Int) -> Double {
    let gramsOfCoffeeAsDouble = Double(gramsOfCoffee)
    return gramsOfWater * gramsOfCoffeeAsDouble
  }
}
